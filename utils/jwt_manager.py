from jwt import encode, decode


def create_token(data: dict):
    token: str = encode(payload=data, key="0304", algorithm="HS256")
    return token


def validate_token(token: str) -> dict:
    decoded_token: dict = decode(token, key="0304", algorithms=["HS256"])
    return decoded_token
